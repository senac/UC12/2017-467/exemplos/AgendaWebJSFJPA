package br.com.senac.sysagenda.bean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named(value = "helloBean")
@RequestScoped
public class HelloBean {

    private String nome;

    public HelloBean() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String vai(){
        
        return null;
    }
    

}
